#include "KMUMU_analysis.hh"

#include "Event.hh"
#include "Persistency.hh"
#include "functions.hh"
#include "KaonDecayConstants.hh"
#include "PnnAnalysisTools.hh"
#include "GeometricAcceptance.hh"

#include <iostream>
#include <stdlib.h>
using namespace NA62Analysis;



KMUMU_analysis::KMUMU_analysis(Core::BaseAnalysis *ba): Analyzer(ba, "KMUMU_analysis") {

    RequestTree("Spectrometer",new TRecoSpectrometerEvent);

    AddParam("IsKSBeam", &fIsKSBeam, true);  // if true histos for KS beam mode
    AddParam("IsKLBeam", &fIsKLBeam, false); // if true, histos for KL beam mode
    AddParam("zTarget", &fTargetZ, 113000); // [mm] z position of the target (default 113m from T10 of NA62)

    fTool = PnnAnalysisTools::GetInstance();
}

void KMUMU_analysis::InitOutput() {

}

void KMUMU_analysis::InitHist() {

    BookHisto(new TH1I("Ntracks", "", 20, 0, 20));
    if(fIsKSBeam){

        BookHisto(new TH1I("Prod_KS_zvtx", "Z_{true vtx} [m]", 240, 100, 220));
        BookHisto(new TH2F("Prod_KS_xyvtx", ";X_{true vtx} [mm]; Y_{true vtx} [mm]", 1500 , -1500, 1500,1500 , -1500, 1500));
        BookHisto(new TH2F("Prod_KS_xzvtx", ";Z_{true vtx} [m]; X_{true vtx} [mm]", 240, 100, 220, 1500 , -1500, 1500));
        BookHisto(new TH2F("Prod_KS_yzvtx", ";Z_{true vtx} [m]; Y_{true vtx} [mm]", 240, 100, 220, 1500 , -1500, 1500));
        BookHisto(new TH1F("Prod_PKS", "", 800, 0, 400));
        BookHisto(new TH2F("Prod_KS_PKS_vs_zvtx", ";Z_{true vtx} [m]; P_{K_{S}} [mm]",240, 100, 220, 800, 0, 400));
    }

    if (fIsKLBeam){

        BookHisto(new TH1I("Prod_KL_zvtx", "Z_{true vtx} [m]", 240, 100, 220));
        BookHisto(new TH2F("Prod_KL_xyvtx", ";X_{true vtx} [mm]; Y_{true vtx} [mm]", 1500 , -1500, 1500,1500 , -1500, 1500));
        BookHisto(new TH2F("Prod_KL_xzvtx", ";Z_{true vtx} [m]; X_{true vtx} [mm]", 240, 100, 220, 1500 , -1500, 1500));
        BookHisto(new TH2F("Prod_KL_yzvtx", ";Z_{true vtx} [m]; Y_{true vtx} [mm]", 240, 100, 220, 1500 , -1500, 1500));
        BookHisto(new TH1F("Prod_PKL", "", 800, 0, 400));
        BookHisto(new TH2F("Prod_KL_PKL_vs_zvtx", ";Z_{true vtx} [m]; P_{K_{L}} [mm]",240, 100, 220, 800, 0, 400));
    }

    BookHisto(new TH1I("PDG_all", "", 1000, -500, 500));

    BookHisto(new TH1F("Pdecay_correction_factor", "N_{decayed K}/N_{produced K}; \gamma_{K true}", 810,0,810));
    BookHisto(new TH1F("PK_reweighted", "", 800, 0, 400));

    BookHisto(new TH1F("Reco_Pplus", "", 600, 0, 300));
    BookHisto(new TH1F("Reco_Pminus", "", 600, 0, 300));
    BookHisto(new TH1F("Reco_Pk", "", 800, 0, 400));
    BookHisto(new TH1F("Reco_Mk", "", 500, 0, 1));
    BookHisto(new TH2F("Reco_piplus_xy_straw1", ";X_{STRAW1} [mm]; Y_{STRAW1} [mm]", 1500 , -1500, 1500,1500 , -1500, 1500));
    BookHisto(new TH2F("Reco_piminus_xy_straw1", ";X_{STRAW1} [mm]; Y_{STRAW1} [mm]", 1500 , -1500, 1500,1500 , -1500, 1500));
    BookHisto(new TH2F("Reco_plus_vs_minus_Rstraw1", ";R_{STRAW1 q=-1} [mm]; R_{STRAW1 q=1} [mm]", 1500 , 0, 1500,1500 , 0, 1500));
    BookHisto(new TH2F("Reco_geom_cda_vs_zvtx", "; Z_{reco vtx} [m];CDA [mm]", 240, 100, 220, 200,0, 200));
    BookHisto(new TH2F("Reco_geom_pk_vs_zvtx", "; Z_{reco vtx} [m]; P_K [GeV/c]", 240, 100, 220, 800,0, 400));
    BookHisto(new TH1F("Reco_geom_dzvtx", "; Z_{reco vtx} - Z_{true vtx} [m]", 500, -50, 50));
    BookHisto(new TH1F("Reco_geom_Pplus", "", 600, 0, 300));
    BookHisto(new TH1F("Reco_geom_Pminus", "", 600, 0, 300));
    BookHisto(new TH1F("Reco_geom_Pk", "", 800, 0, 400));
    BookHisto(new TH1F("Reco_geom_Mk", "", 500, 0, 1));

    BookHisto(new TH1I("Reco_geom_PDG", "", 1000, -500, 500));
    BookHisto(new TH1I("Reco_geom_NKineParts", "", 10, -0.5, 9.5));

    BookHisto(new TH2F("Reco_geom_dPk_vs_Pktrue", ";P_{K true} [GeV/c];P_{K reco} - P_{K true} [GeV/c]", 800, 0, 400, 1000, -10,10));
    BookHisto(new TH2F("Reco_geom_dThetak_vs_Pktrue", ";P_{K true} [GeV/c];#Theta_{K reco} - #Theta_{K true} [rad]", 800, 0, 400, 1000, -0.001,0.001));

    BookHisto(new TH2F("Reco_geom_dPplus_vs_Pplus_true", ";P_{plus true} [GeV/c];P_{plus reco} - P_{plus true} [GeV/c]", 800, 0, 400, 800, -50,50));
    BookHisto(new TH2F("Reco_geom_dPminus_vs_Pminus_true", ";P_{minus true} [GeV/c];P_{minus reco} - P_{minus true} [GeV/c]", 800, 0, 400, 1000, -10,10));
    BookHisto(new TH2F("Reco_geom_dTheta_minus_vs_Theta_minus_true", ";#Theta_{minus true} [rad];#Theta_{minus reco} - #Theta_{minus true} [rad]", 1000, 0,0.02, 1000, -0.001,0.001));
    BookHisto(new TH2F("Reco_geom_dTheta_minus_vs_Pminus_true", ";P_{minus true} [GeV/c];#Theta_{minus reco} - #Theta_{minus true} [rad]", 800, 0, 400, 1000, -0.001,0.001));
    BookHisto(new TH2F("Reco_geom_dTheta_plus_vs_Theta_plus_true", ";#Theta_{plus true} [rad];#Theta_{plus reco} - #Theta_{plus true} [rad]", 1000, 0,0.02, 1000, -0.001,0.001));
    BookHisto(new TH2F("Reco_geom_dTheta_plus_vs_Pplus_true", ";P_{plus true} [GeV/c];#Theta_{plus reco} - #Theta_{plus true} [rad]", 800, 0, 400, 1000, -0.001,0.001));


    BookHisto(new TH1I("Reco_geom_Npidecays_before_straw4", "", 3, -0.5, 2.5));

    BookHisto(new TH2F("Reco_geom_gammaK_vs_Pk", "", 800, 0, 400, 800,0,400));
    BookHisto(new TH2F("Reco_geom_gammaK_true_vs_Pk_true", "", 800, 0, 400, 800,0,400));
    BookHisto(new TH2F("Reco_geom_dgammaK_vs_Pk_true", "", 800,0, 400, 200, -20, 20));

    BookHisto(new TH1F("Reco_final_Pplus", "", 600, 0, 300));
    BookHisto(new TH1F("Reco_final_Pminus", "", 600, 0, 300));
    BookHisto(new TH1F("Reco_final_Pk", "", 800, 0, 400));
    BookHisto(new TH1F("Reco_final_Mk", "", 500, 0.4, 0.6));
    BookHisto(new TH2F("Reco_final_cda_vs_zvtx", "; Z_{reco vtx} [m];CDA [mm]", 240, 100, 220, 200,0, 200));
    BookHisto(new TH2F("Reco_final_dPk_vs_Pktrue", ";P_{K true} [GeV/c];P_{K reco} - P_{K true} [GeV/c]", 800, 0, 400, 1000, -10,10));
    BookHisto(new TH2F("Reco_final_dThetak_vs_Pktrue", ";P_{K true} [GeV/c];#Theta_{K reco} - #Theta_{K true} [rad]", 800, 0, 400, 1000, -0.001,0.001));

    BookHisto(new TH2F("Reco_final_tauks_vs_Pk", "; P_{K reco} [GeV/c]; t [#tau_{K_{S}}]", 800, 0, 400, 200, 0, 50));
    BookHisto(new TH2F("Reco_final_tauks_vs_zvtx", "; Z_{vtx reco} [m]; t [#tau_{K_{S}}]", 240, 100, 220, 200, 0, 50));

    BookHisto(new TH2F("Reco_final_dtauks_vs_Pk_true", "; P_{K true} [GeV/c]; t [#tau_{K_{S}}]", 800, 0, 400, 400,-10,10));
    BookHisto(new TH2F("Reco_final_dtauks_vs_zvtx_true", "; Z_{vtx true} [m]; t [#tau_{K_{S}}]", 240, 100, 220, 400, -10, 10));
    BookHisto(new TH2F("Reco_final_dgammaK_vs_Pk_true", "", 800,0, 400, 200, -20, 20));

    BookHisto(new TH2F("Reco_final_Mk_vs_Pk", "; P_{K reco} [GeV/c]; M_{K reco} [GeV/c^{2}]", 800, 0, 400, 500, 0.4, 0.6));
    BookHisto(new TH2F("Reco_final_dMk_vs_Pk_true", "; P_{K reco} [GeV/c]; M_{K reco} - M_{K true} [GeV/c^{2}]", 800, 0, 400, 500, -0.05, 0.05));
    BookHisto(new TH2F("Reco_final_dMk_vs_Thetak_true", "; #Theta_{K true} [rad]; M_{K reco} - M_{K true} [GeV/c^{2}]",200, 0,0.001, 500, -0.05, 0.05));

}

void KMUMU_analysis::StartOfRunUser() {

}

void KMUMU_analysis::Clear() {

    fTp = TLorentzVector(0.0,0.0,0.0,0.0);
    fTm = TLorentzVector(0.0,0.0,0.0,0.0);
    fTwoTrack = TLorentzVector(0.0,0.0,0.0,0.0);
    fTrueVertex = TLorentzVector(0.0,0.0,0.0,0.0);
    fRecoVertex = TVector3(0.0,0.0,0.0);
    fSpectrometerEvent = NULL;
    fSplus = NULL;
    fSminus = NULL;
    fCDA = 99999.;
    fBeamParticle = NULL;
    fctau_ks = 0;
    fctau_kl = 0;
}

void KMUMU_analysis::StartOfBurstUser() {

}

void KMUMU_analysis::ProcessSpecialTriggerUser(int iEvent, unsigned int triggerType) {

}

void KMUMU_analysis::Process(int iEvent) {

    Clear();
    //fctau_ks = 0.026844; //[m]
    // fctau_kl = 15.34; //[m]
    fctau_ks = 26.844; //[mm]
    fctau_kl = 15340; //[mm]

    fSpectrometerEvent = (TRecoSpectrometerEvent*)GetEvent("Spectrometer");

    FillHisto("Ntracks", fSpectrometerEvent->GetNCandidates());

    Event *evt = GetMCEvent();
    TIter partit_all(evt->GetKineParts());
    while(partit_all.Next()) {  // itterate through ALL particles
        KinePart *particle = static_cast<KinePart *>(*partit_all);
        if(particle->GetPDGcode() == 310) {  // KS
            fTrueVertex = particle->GetEndPos();
            fBeamParticle = particle;
            FillHisto("Prod_KS_zvtx", fTrueVertex.Z()/1e3);
            FillHisto("Prod_KS_xzvtx", particle->GetEndPos().Z()/1e3, particle->GetEndPos().X());
            FillHisto("Prod_KS_yzvtx", particle->GetEndPos().Z()/1e3, particle->GetEndPos().Y());
            FillHisto("Prod_KS_xyvtx", particle->GetEndPos().X(), particle->GetEndPos().Y());
            FillHisto("Prod_PKS", particle->GetInitial4Momentum().P()/1e3);
            FillHisto("Prod_KS_PKS_vs_zvtx", fTrueVertex.Z()/1e3, particle->GetInitial4Momentum().P()/1e3);
        }
        if(particle->GetPDGcode() == 130) {  // KL
            fTrueVertex = particle->GetEndPos();
            fBeamParticle = particle;
            FillHisto("Prod_KL_zvtx", fTrueVertex.Z()/1e3);
            FillHisto("Prod_KL_xzvtx", particle->GetEndPos().Z()/1e3, particle->GetEndPos().X());
            FillHisto("Prod_KL_yzvtx", particle->GetEndPos().Z()/1e3, particle->GetEndPos().Y());
            FillHisto("Prod_KL_xyvtx", particle->GetEndPos().X(), particle->GetEndPos().Y());
            FillHisto("Prod_PKL", particle->GetInitial4Momentum().P()/1e3);
            FillHisto("Prod_KL_PKL_vs_zvtx", fTrueVertex.Z()/1e3, particle->GetInitial4Momentum().P()/1e3);
        }
        FillHisto("PDG_all", particle->GetPDGcode());
        //if(particle->GetPDGcode() == 211) {  // pi+
        //    fTp = particle->GetFinal4Momentum();
        //}
        //if(particle->GetPDGcode() == -211) {  // pi-
        //   fTm = particle->GetFinal4Momentum();
        //}

        //std::cout << "PDG = " << particle->GetPDGcode() << " Endpos = " << particle->GetEndPos().Z() << " P =  " << particle->GetInitial4Momentum().Px() << "  " << particle->GetInitial4Momentum().Py() << "   " <<  particle->GetInitial4Momentum().Pz() << "   " << particle->GetInitial4Momentum().P()  << " M = " << particle->GetFinal4Momentum().M() << std::endl;
    }

    // if(fTrueVertex.Z() < 120000 || fTrueVertex.Z() > 198000) return; //FV between 120m and 198m (target is at 113m and STRAW at 213 m)
    if(fTrueVertex.Z() < fTargetZ || fTrueVertex.Z() > 198000) return; //FV between 120m and 198m (target is at 113m and STRAW at 213 m)
    //if(fTrueVertex.Z() >= 120000) return; //used to compute the fraction of particles lost due to collimation

    double beta_K_true    = (fBeamParticle->GetInitial4Momentum().P()/1e3)/TMath::Sqrt( (fBeamParticle->GetInitial4Momentum().P()/1e3)*(fBeamParticle->GetInitial4Momentum().P()/1e3) + MK0/1000.*MK0/1000.);
    double gamma_K_true   = 1/TMath::Sqrt(1 - beta_K_true*beta_K_true);
    double tau_ks_true    = (fTrueVertex.Z() - fTargetZ)/(fctau_ks*gamma_K_true);

    //Weight used to compute the total amount of KL that correspond to the generated KL decays
    //fdecay : Probability that the kaon that is generated at the primary target would have decayed within the defined fiducial volume
    double fdecay = -1;
    if(fIsKLBeam)
        fdecay = 1 - TMath::Exp(- (198000 - fTargetZ)/(fctau_kl*gamma_K_true));
        //fdecay = 1 - TMath::Exp(- (fTrueVertex.Z() - fTargetZ)/(fctau_kl*gamma_K_true));
    if(fIsKSBeam)
        fdecay = 1 - TMath::Exp(- (198000 - fTargetZ)/(fctau_ks*gamma_K_true));

    //std::cout << "KS - " <<fIsKSBeam << " KL - " << fIsKLBeam << " gamma_K = " << gamma_K_true <<  " zvtx = " << fTrueVertex.Z() << " fdecay = " << fdecay <<  std::endl;
    FillHisto("Pdecay_correction_factor",gamma_K_true, fdecay );
    FillHisto("PK_reweighted", fBeamParticle->GetInitial4Momentum().P()/1e3, 1/fdecay);

    if(fSpectrometerEvent->GetNCandidates()!= 2) return;
    double qvertex = 0;
    for (int iT(0); iT < fSpectrometerEvent->GetNCandidates();iT++){
        TRecoSpectrometerCandidate *pT = static_cast<TRecoSpectrometerCandidate *>(fSpectrometerEvent->GetCandidate(iT));
        //std::cout << "Q = " << pT->GetCharge() << std::endl;
        qvertex += pT->GetCharge();
        if(pT->GetCharge()==1){
            FillHisto("Reco_Pplus", pT->GetMomentum()/1000.);

            fTp = GetTrack(pT,MPI/1000.);
            fSplus  = pT;
        }
        if(pT->GetCharge()==-1){
            FillHisto("Reco_Pminus", pT->GetMomentum()/1000.);

            fTm = GetTrack(pT,MPI/1000.);
            fSminus  = pT;
            //std::cout << MPI << "MK0 = " << MK0 << std::endl;
        }
    }

    if(qvertex != 0) return;

    TVector3 posstraw1_plus = fTool->GetPositionAtZ(fSplus, GeometricAcceptance::GetInstance()->GetZStraw(0));
    TVector3 posstraw2_plus = fTool->GetPositionAtZ(fSplus, GeometricAcceptance::GetInstance()->GetZStraw(1));
    TVector3 posstraw3_plus = fTool->GetPositionAtZ(fSplus, GeometricAcceptance::GetInstance()->GetZStraw(2));
    TVector3 posstraw4_plus = fTool->GetPositionAtZ(fSplus, GeometricAcceptance::GetInstance()->GetZStraw(3));



    TVector3 posstraw1_minus = fTool->GetPositionAtZ(fSminus, GeometricAcceptance::GetInstance()->GetZStraw(0));
    TVector3 posstraw2_minus = fTool->GetPositionAtZ(fSminus, GeometricAcceptance::GetInstance()->GetZStraw(1));
    TVector3 posstraw3_minus = fTool->GetPositionAtZ(fSminus, GeometricAcceptance::GetInstance()->GetZStraw(2));
    TVector3 posstraw4_minus = fTool->GetPositionAtZ(fSminus, GeometricAcceptance::GetInstance()->GetZStraw(3));

    double rstraw1_plus  = TMath::Sqrt(TMath::Power(posstraw1_plus.X(),2)  + TMath::Power(posstraw1_plus.Y(),2) );
    double rstraw1_minus = TMath::Sqrt(TMath::Power(posstraw1_minus.X(),2) + TMath::Power(posstraw1_minus.Y(),2) );
    fTwoTrack.SetPxPyPzE(fTp.Px()+fTm.Px(), fTp.Py()+fTm.Py(), fTp.Pz()+fTm.Pz(), fTp.E()+fTm.E());

    FillHisto("Reco_piplus_xy_straw1" , posstraw1_plus.X(), posstraw1_plus.Y());
    FillHisto("Reco_piminus_xy_straw1", posstraw1_minus.X(), posstraw1_minus.Y());
    FillHisto("Reco_plus_vs_minus_Rstraw1", rstraw1_minus, rstraw1_plus );
    FillHisto("Reco_Mk", fTwoTrack.M());
    FillHisto("Reco_Pk", fTwoTrack.P());

    //std::cout <<  << std::endl;

    if(rstraw1_minus < 200 || rstraw1_plus < 200 ) return;
    // fTwoTrack = fTp + fTm;

    //std::cout << "AA Momentum =" << fTp.P() << "  " << fTm.P() << "  " << fTwoTrack.P() << std::endl;
    //std::cout << " ----------------------" << GetEventHeader()->GetEventNumber() << std::endl;
    //std::cout << "TP X,Y,Z    =" << fTp.Vect().X() << "  " << fTp.Vect().Y() << "  " << fTp.Vect().Z() << std::endl;
    //std::cout << "TM X,Y,Z    =" << fTm.Vect().X() << "  " << fTm.Vect().Y() << "  " << fTm.Vect().Z()  << std::endl;
    //std::cout << "PP1 X,Y,Z    =" << posstraw1_plus.X() << "  " << posstraw1_plus.Y() << "  " << posstraw1_plus.Z() << " CDA = " << fCDA  << std::endl;
    //std::cout << "PM1 X,Y,Z    =" << posstraw1_minus.X() << "  " << posstraw1_minus.Y() << "  " << posstraw1_minus.Z() << " CDA = " << fCDA  << std::endl;
    //std::cout << "STRackP_PS1 X,Y,Z    =" << fSplus->GetPositionBeforeGet.X() << "  " << posstraw1_plus.Y() << "  " << posstraw1_plus.Z() << " CDA = " << fCDA  << std::endl;
    //std::cout << "STrackM_PS1 X,Y,Z    =" << posstraw1_minus.X() << "  " << posstraw1_minus.Y() << "  " << posstraw1_minus.Z() << " CDA = " << fCDA  << std::endl;
  
    fCDA = 999.0;
    fRecoVertex = fTool->SingleTrackVertex(fTp.Vect(),fTm.Vect(),posstraw1_plus, posstraw1_minus, fCDA);
    //std::cout<< "Recovtx = " << fRecoVertex.X() << "  " <<  fRecoVertex.Y() << " " << fRecoVertex.Z()  << "CDA = " <<  fCDA << std::endl;
    FillHisto("Reco_geom_cda_vs_zvtx", fRecoVertex.Z()/1e3, fCDA);
    FillHisto("Reco_geom_pk_vs_zvtx", fRecoVertex.Z()/1e3, fTwoTrack.P());
    FillHisto("Reco_geom_dzvtx", (fRecoVertex.Z()- fTrueVertex.Z())/1e3);
    FillHisto("Reco_geom_Pk", fTwoTrack.P());
    FillHisto("Reco_geom_Mk", fTwoTrack.M());
    FillHisto("Reco_geom_Pplus", fTp.P());
    FillHisto("Reco_geom_Pminus", fTm.P());

    FillHisto("Reco_geom_dPk_vs_Pktrue",fBeamParticle->GetInitial4Momentum().P()/1e3, fTwoTrack.P() - fBeamParticle->GetInitial4Momentum().P()/1e3);
    FillHisto("Reco_geom_dThetak_vs_Pktrue",fBeamParticle->GetInitial4Momentum().P()/1e3, fTwoTrack.Theta() - fBeamParticle->GetInitial4Momentum().Theta());



    TIter partit_reco_geom(evt->GetKineParts());
    int npidec_bstraw4 = 0; // straw 1,2,3,4 = [212.9, 219.5, 229.8, 236.8] m
    while(partit_reco_geom.Next()) {  // itterate through ALL particles
        KinePart *particle = static_cast<KinePart *>(*partit_reco_geom);
        if(particle->GetPDGcode() != 130 && particle->GetPDGcode() != 310)
            FillHisto("Reco_geom_PDG", particle->GetPDGcode());


        if(particle->GetPDGcode() == 211 ){
            if(particle->GetEndPos().Z() < 237000){
                npidec_bstraw4 += 1;

            } else {

                FillHisto("Reco_geom_dPplus_vs_Pplus_true", particle->GetInitial4Momentum().P()/1e3, fTp.P() - particle->GetInitial4Momentum().P()/1e3 );
                FillHisto("Reco_geom_dTheta_plus_vs_Theta_plus_true", particle->GetInitial4Momentum().Theta(), fTp.Theta() - particle->GetInitial4Momentum().Theta() );
                FillHisto("Reco_geom_dTheta_plus_vs_Pplus_true",particle->GetInitial4Momentum().P()/1e3, fTp.Theta() - particle->GetInitial4Momentum().Theta() );
            }

        }

        if(particle->GetPDGcode() == -211 ){
            if(particle->GetEndPos().Z() < 237000){
                npidec_bstraw4 += 1;

            } else {
                FillHisto("Reco_geom_dPminus_vs_Pminus_true", particle->GetInitial4Momentum().P()/1e3, fTm.P() - particle->GetInitial4Momentum().P()/1e3 );
                FillHisto("Reco_geom_dTheta_minus_vs_Theta_minus_true", particle->GetInitial4Momentum().Theta(), fTm.Theta() - particle->GetInitial4Momentum().Theta() );
                FillHisto("Reco_geom_dTheta_minus_vs_Pminus_true", particle->GetInitial4Momentum().P()/1e3, fTm.Theta() - particle->GetInitial4Momentum().Theta() );
            }

        }



    }

    FillHisto("Reco_geom_Npidecays_before_straw4", npidec_bstraw4);
    FillHisto("Reco_geom_NKineParts", evt->GetNKineParts());

    double beta_K    = fTwoTrack.P()/TMath::Sqrt(fTwoTrack.P()*fTwoTrack.P() + MK0/1000.*MK0/1000.);
    //double beta_K    = pk_sim/TMath::Sqrt(pk_sim*pk_sim + MLAMBDA_GEV*MLAMBDA_GEV);
    double gamma_K   = 1/TMath::Sqrt(1 - beta_K*beta_K);
    double tau_ks    = (fRecoVertex.Z() - fTargetZ)/(fctau_ks*gamma_K);


    FillHisto("Reco_geom_gammaK_vs_Pk", fTwoTrack.P(), gamma_K);
    FillHisto("Reco_geom_gammaK_true_vs_Pk_true", fBeamParticle->GetInitial4Momentum().P()/1e3, gamma_K_true);
    FillHisto("Reco_geom_dgammaK_vs_Pk_true", fBeamParticle->GetInitial4Momentum().P()/1e3, gamma_K - gamma_K_true);



    if(npidec_bstraw4 > 0) return;
    FillHisto("Reco_final_Pk", fTwoTrack.P());
    FillHisto("Reco_final_Mk", fTwoTrack.M());
    FillHisto("Reco_final_Mk_vs_Pk",fTwoTrack.P() ,fTwoTrack.M());
    FillHisto("Reco_final_Pplus", fTp.P());
    FillHisto("Reco_final_Pminus", fTm.P());

    FillHisto("Reco_final_dPk_vs_Pktrue",fBeamParticle->GetInitial4Momentum().P()/1e3, fTwoTrack.P() - fBeamParticle->GetInitial4Momentum().P()/1e3);
    FillHisto("Reco_final_dThetak_vs_Pktrue",fBeamParticle->GetInitial4Momentum().P()/1e3, fTwoTrack.Theta() - fBeamParticle->GetInitial4Momentum().Theta());

    FillHisto("Reco_final_tauks_vs_Pk", fTwoTrack.P(), tau_ks);
    FillHisto("Reco_final_tauks_vs_zvtx", fRecoVertex.Z()/1e3, tau_ks);
    FillHisto("Reco_final_dtauks_vs_zvtx_true", fTrueVertex.Z()/1e3, tau_ks - tau_ks_true);
    FillHisto("Reco_final_dtauks_vs_Pk_true", fBeamParticle->GetInitial4Momentum().P()/1e3, tau_ks - tau_ks_true);
    FillHisto("Reco_final_cda_vs_zvtx", fRecoVertex.Z()/1e3, fCDA);
    FillHisto("Reco_final_dgammaK_vs_Pk_true", fBeamParticle->GetInitial4Momentum().P()/1e3, gamma_K - gamma_K_true);

    FillHisto("Reco_final_dMk_vs_Pk_true",fBeamParticle->GetInitial4Momentum().P()/1e3 ,fTwoTrack.M() - MK0_GEV);
    FillHisto("Reco_final_dMk_vs_Thetak_true",fBeamParticle->GetInitial4Momentum().Theta() ,fTwoTrack.M() - MK0_GEV);
    return;
}

TLorentzVector KMUMU_analysis::GetTrack(TRecoSpectrometerCandidate* tr, Double_t mass){

    TLorentzVector track;
    //double px = tr->GetThreeMomentumBeforeMagnet().X() * 0.001;
    //double py = tr->GetThreeMomentumBeforeMagnet().Y() * 0.001;
    //double pz = tr->GetThreeMomentumBeforeMagnet().Z() * 0.001;
    //
    //double E  = TMath::Sqrt(px*px + py*py + pz*pz + mass*mass);
    //std::cout << " Momentum =" << px << "  " << py << "  " << pz << " Compute = " << TMath::Sqrt(px*px+py*py+pz*pz) << "From Reco = " << tr->GetMomentum()/1000.<< "  "<< mass << " E = " << E << std::endl;


    // track.SetPxPyPzE(px,py,pz,E);
    track.SetVectM(tr->GetThreeMomentumBeforeMagnet()*0.001,mass);

    return track;
}
void KMUMU_analysis::PostProcess() {
}

void KMUMU_analysis::EndOfBurstUser() {
}

void KMUMU_analysis::EndOfRunUser() {
}

void KMUMU_analysis::EndOfJobUser() {
    SaveAllPlots();
}

void KMUMU_analysis::DrawPlot() {
}

KMUMU_analysis::~KMUMU_analysis() {
}
